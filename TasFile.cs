using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ZeepSDK.Messaging;

namespace TASkist;

public class TasFile
{
    List<TasInput> inputs;
    int index;

    public TasFile(string  path)
    {
        inputs = new List<TasInput>();
        inputs.Add(new TasInput());

        TasInput last = new TasInput();
        inputs.Add(new TasInput());

        foreach (string line in File.ReadLines(path))
        {
            if (line.StartsWith("//"))
            {
                continue;
            }
            string[] inputs = line.Split(';');
            TasInput curr = new TasInput();
            for (int i = 0; i < 4; i++)
            {
                string input = inputs[i].Trim();

                switch (i)
                {
                    case 0:
                        curr.time = (input == "." ? last.time : float.Parse(input));
                        continue;
                    case 1:
                        curr.steering = (input == "." ? last.steering : float.Parse(input) / 100);
                        continue;
                    case 2:
                        curr.arms = (input == "." ? last.arms : input == "d");
                        continue;
                    case 3:
                        curr.brake = (input == "." ? last.brake : input == "d");
                        continue;
                    default:
                        break;
                }
            }
            last = curr;
            this.inputs.Add(curr);
        }
    }

    public TasInput getInput(float time)
    {

        if (index + 1 == inputs.Count) {
            return inputs[index]; 
        } 

        for(int i = 0; i < inputs.Count; i++)
        {
            if (time >= inputs[i].time && i != index) {
                index = i;
                if (Plugin.EnableLog.Value)
                {
                    Plugin.Log.LogInfo("ADVANCING INPUT");
                    Plugin.Log.LogInfo(inputs[index]);
                }
            }
        }
        return inputs[index];
    }

    public void setIndex(int i) {
        if (i >= inputs.Count) {
            index = inputs.Count - 1;
        }else {
        
            index = i;
        }
    } 

    public CarState saveState(GameMaster instance, float time)
    {
        SetupCar carSetup = instance.carSetups[0];
        CarState state = new CarState()
        {
            carPosition = carSetup.transform.position,
            carRotation = carSetup.transform.rotation,
            carVelocity = carSetup.cc.GetRB().velocity,
            carAngularVelocity = carSetup.cc.GetRB().angularVelocity,
            time = time,
            index = index,
        };
        // MessengerApi.Log("saved state");
        return state;
    }

    internal void setState(GameMaster instance, CarState state)
    {
        SetupCar carSetup = instance.carSetups[0];

        carSetup.transform.position = state.carPosition;
        carSetup.transform.rotation = state.carRotation;
        carSetup.cc.GetRB().velocity = state.carVelocity;
        carSetup.cc.GetRB().angularVelocity = state.carAngularVelocity;

        instance.PlayersReady[0].ticker.what_ticker = state.time;

        index = state.index;
        // MessengerApi.Log("load state");
    }

    internal void injectInput(GameMaster instance)
    {
        if (instance.PlayersReady.Count <= 0)
        {
            return;
        }

        New_ControlCar cc = instance.PlayersReady[0].cc;
        TasInput input = getInput(TasManager.getTime());

        cc.SteerAction.axis = input.steering;
        cc.BrakeAction.buttonHeld = input.brake;
        cc.BrakeAction.axis = input.brake ? 1f : 0f;
        cc.ArmsUpAction.buttonHeld = input.arms;

    }

    internal bool IsChanged()
    {
        throw new NotImplementedException();
    }
}

public class CarState
{
    public Vector3 carPosition;
    public Quaternion carRotation;
    public Vector3 carVelocity;
    public Vector3 carAngularVelocity;
    public float time;
    public int index;
}

public class TasInput
{
    public float time { get; internal set; } = 0; // micro secconds, thats the precision the game uses
    public float steering { get; internal set; } = 0;
    public bool arms { get; internal set; } = false;
    public bool brake { get; internal set; } = false;

    public TasInput() { }

    public TasInput(float time, float steering, bool arms, bool brake)
    {
        this.time = time;
        this.steering = steering;
        this.arms = arms;
        this.brake = brake;
    }

    public override string ToString()
    {
        return "[" + time + "," + steering + "," + arms + "," + brake + "]";
    }
}
