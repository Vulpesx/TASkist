# TASkist

<!--toc:start-->
- [TASkist](#taskist)
  - [Description](#description)
  - [Getting Started](#getting-started)
    - [Installation](#installation)
    - [Using](#using)
  - [Author](#author)
  - [Version History](#version-history)
<!--toc:end-->

## Description
TASkist is a tool for creating Tool Assisted Speedruns in the game [Zeepkist](https://store.steampowered.com/app/1440670?snr=5000_5100___primarylinks).
It is currently very simple, it modifies the inputs based on a .tas file. 
Eventualy you will be able to make TAS' in game but for now you have to edit .tas files manualy.
An external editor will be available soon.

## Getting Started

### Installation
TASkist will be available using the [Modkist](https://mod.io/g/zeepkist/r/mod-manager) mod manager but until then follow these steps:

1) First make sure you install [BepInEx](https://docs.bepinex.dev/articles/user_guide/installation/index.html)
if you currently are using mods in Zeepkist it will already be installed. *Modkist will install it for you*

2) download the dll file from the Releases.

3) in steam right click on Zeepkist go to `Managed`->`Browse lcal files`. you should see a BepInEx folder. 

4) copy/move the dll you downloaded into `BepInEx`->`plugins`. it doesnt have to be in its own folder like how Modkist
does things.

### Usage
load a level in freeplay. change settings to load .tas file *you have to create new files*
create/edit the .tas file. *[a shitty editor](https://codeberg.org/Vulpesx/taskist-editor)*

```tas
// is comment
// fields are separated with ;
// all fields except [ms] can be marked unchanged with '.' default is 0; 0; u; u;
// there are four fields
//sec  ; steering ; arms ; brake ; anything past here is a comment 
0.1    ;     .    ;  .   ;   .   ; this example is 0.1 secconds 
1      ;   -20    ;  .   ;   .   ; at 1s steer -20% must be between [-100:100]
2      ;    5     ;  d   ;   .   ; at 2s +5% steer and arms up, d means button pressed, u for up unpressed
5      ;    0     ;  u   ;   d   ; the last inputs given will stay till we change them
```
restart the level to reload the file.

#### Config
To change the settings, you should install [Zeep Settings](https://mod.io/g/zeepkist/m/zeep-settings) *manualy or with Modkist*
That way there should be a mod section in the normal settings.

Settings:
- Tas Dir:
  directory where you tas files are
- Tas File:
  Tas file you are using
- Load Tas:
  load tas file
- Reload Tas:
  reload tas file automagicaly
- State on Reload:
  load state if there is one on file reload
- Disable:
  Disable TASkist
- Enable Log:
  Enable Logging
- Save State:
  Saves you current position, rotation, velocity and angulary velocity
- Load State:
  apply you save state if there is one


## Author
me

## Version History
- 1.0.0
  * the basics
- 1.1.0
  * fixed bug where TASkist would use the first input imediately, instead of waiting
  * reorginze my code
  * Save states
  * automagical file reloading
  * changed tas file format

## License
This project is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0) license
