﻿using System;
using System.Collections.Generic;
using System.IO;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using UnityEngine;
using ZeepSDK.Messaging;

// using ZeepkistClient;

namespace TASkist;

[BepInPlugin(MyPluginInfo.PLUGIN_GUID, MyPluginInfo.PLUGIN_NAME, MyPluginInfo.PLUGIN_VERSION)]
[BepInDependency("ZeepSDK")]
public class Plugin : BaseUnityPlugin
{
    private Harmony harmony;

    internal static new ManualLogSource Log;
    public static bool SettingsChanged { get; set; } = false;

    public static ConfigEntry<bool> Disabled { get; private set; }

    public static ConfigEntry<bool> EnableLog { get; private set; }

    public static ConfigEntry<bool> LoadReload { get; private set; }
    public static ConfigEntry<bool> Reload { get; private set; }

    public static ConfigEntry<KeyCode> LoadTas { get; private set; }
    public static ConfigEntry<KeyCode> SaveState { get; private set; }
    public static ConfigEntry<KeyCode> LoadState { get; private set; }

    private void Awake()
    {
        Log = base.Logger;

        harmony = new Harmony(MyPluginInfo.PLUGIN_GUID);
        harmony.PatchAll(typeof(GameMasterPatch));

        // Plugin startup logic
        Logger.LogInfo($"Plugin {MyPluginInfo.PLUGIN_GUID} is loaded!");

        ConfigSetup();

        TasManager.TasDirectory.SettingChanged += Plugin.setChanged;
        TasManager.TasFile.SettingChanged += Plugin.setChanged;

        Directory.CreateDirectory(TasManager.TasDirectory.Value);
        Logger.LogInfo("initialised Tas folder");
    }

    private void ConfigSetup()
    {
        Disabled = base.Config.Bind("General", "Disable", true, "Disable Tas");
        EnableLog = base.Config.Bind("General", "Enable Log", false, "Enable Loging");
        LoadTas = base.Config.Bind("File Load", "Load TAS", KeyCode.L, "loads tas file");
        SaveState = base.Config.Bind("State", "Save State", KeyCode.X, "save state keybind");
        LoadState = base.Config.Bind("State", "Load State", KeyCode.C, "load state keybind");
        Reload = base.Config.Bind("File Load", "Reload TAS", false, "Reload Tas on change");
        LoadReload = base.Config.Bind(
            "File Load",
            "State on Reload",
            true,
            "load state when tas is loaded"
        );

        TasManager.TasDirectory = base.Config.Bind(
            "File",
            "TAS Dir",
            Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "Zeepkist",
                "TAS"
            ),
            "Directory to look for TAS files in"
        );
        TasManager.TasFile = base.Config.Bind(
            "File",
            "TAS file",
            "Default.tas",
            "current TAS file"
        );
    }

    private static void setChanged(object sender, EventArgs e)
    {
        Plugin.SettingsChanged = true;
        Plugin.Log.LogDebug("Settings Changed");
    }

    private void OnDestroy()
    {
        harmony?.UnpatchSelf();
        harmony = null;
    }
}
