using System;
using System.IO;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;
using ZeepSDK.Messaging;

// using ZeepkistClient;

namespace TASkist;

public static class TasManager
{
    public static ConfigEntry<string> TasDirectory { get; set; }
    public static ConfigEntry<string> TasFile { get; set; }
    public static TasFile LoadedTas { get; set; }
    private static DateTime LastModified { get; set; }

    public static CarState SaveState { get; set; }

    public static void loadTas()
    {
        try
        {
            String path = Path.Combine(TasDirectory.Value, TasFile.Value);
            LoadedTas = new TasFile(path);
            LastModified = File.GetLastWriteTime(path);
            MessengerApi.LogSuccess("TAS FILE loaded", 10);
            if (Plugin.EnableLog.Value)
            {
                Plugin.Log.LogInfo("TAS FILE loaded successfully");
            }
        }
        catch (Exception e)
        {
            LoadedTas = null;
            MessengerApi.LogError("Error loading TAS FILE");
            Plugin.Log.LogError(e);
        }
    }

    public static bool TasAllowed(LevelScriptableObject GlobalLevel)
    {
        return !ZeepkistClient.ZeepkistNetwork.IsConnected
            && PlayerManager.Instance.singlePlayer
            && !GlobalLevel.IsAdventureLevel
            && !GlobalLevel.IsTestLevel;
    }

    public static float getTime()
    {
        return PlayerManager.Instance.currentMaster.PlayersReady[0].ticker.GetTicker();
    }

    public static bool TasModified()
    {
        return LastModified
            < File.GetLastWriteTime(Path.Combine(TasDirectory.Value, TasFile.Value));
    }
}

[HarmonyPatch(typeof(GameMaster))]
public static class GameMasterPatch
{
    [HarmonyPatch("FixedUpdate")]
    [HarmonyPrefix]
    public static void Update(GameMaster __instance)
    {
        TasFile tas = TasManager.LoadedTas;
        if (tas == null || !TasManager.TasAllowed(__instance.GlobalLevel))
        {
            return;
        }


        CarState state = TasManager.SaveState;

        if ((Plugin.SettingsChanged || TasManager.TasModified()) && Plugin.Reload.Value)
        {
            TasManager.loadTas();
            if (Plugin.LoadReload.Value && state != null)
            {
                tas.setState(__instance, state);
            }
        }
        else if (Input.GetKeyDown(Plugin.LoadTas.Value))
        {
            TasManager.loadTas();
            if (Plugin.LoadReload.Value && state != null)
            {
                tas.setState(__instance, state);
            }
        }

        if (Input.GetKeyDown(Plugin.SaveState.Value))
        {
            TasManager.SaveState = tas.saveState(__instance, TasManager.getTime());
        }
        else if (Input.GetKeyDown(Plugin.LoadState.Value) && state != null)
        {
            tas.setState(__instance, TasManager.SaveState);
        }

        tas.injectInput(__instance);
    }

    [HarmonyPatch(nameof(GameMaster.RestartTheLevel))]
    [HarmonyPostfix]
    static void LoadTas(GameMaster __instance)
    {
        if (Plugin.Disabled.Value)
        {
            return;
        }

        if (TasManager.TasAllowed(__instance.GlobalLevel))
        {
            if (TasManager.LoadedTas == null || Plugin.SettingsChanged || TasManager.TasModified())
            {
                TasManager.loadTas();
                Plugin.SettingsChanged = false;
            } else {
                TasManager.LoadedTas.setIndex(0);
            }
        }
        else
        {
            MessengerApi.LogWarning("TAS disabled on multiplayer");
        }
    }
}
