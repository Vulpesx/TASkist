#!/bin/fish

dotnet build

# this is a unix command (only works on linux/mac) and makes use on symlinks so is not the actual game location
cp ./bin/Debug/net472/(basename (pwd)).dll ~/Games/Zeepkist/steam/BepInEx/scripts/
cp ./*.tas ~/Games/Zeepkist/TAS/
